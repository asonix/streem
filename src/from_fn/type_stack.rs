use std::{
    any::{Any, TypeId},
    collections::HashMap,
};

#[derive(Default)]
pub(super) struct TypeStack {
    inner: HashMap<TypeId, Box<dyn Any>>,
}

impl TypeStack {
    pub(super) fn new() -> Self {
        Self::default()
    }

    pub(super) fn push<T: 'static>(&mut self, item: T) {
        let type_id = TypeId::of::<Vec<T>>();
        let entry = self
            .inner
            .entry(type_id)
            .or_insert_with(|| Box::<Vec<T>>::default());

        entry
            .downcast_mut::<Vec<T>>()
            .expect("vec exists")
            .push(item);
    }

    pub(super) fn get_mut<T: 'static>(&mut self) -> Option<&mut T> {
        let type_id = TypeId::of::<Vec<T>>();
        self.inner
            .get_mut(&type_id)
            .and_then(|entry| entry.downcast_mut::<Vec<T>>())
            .and_then(|vec| vec.last_mut())
    }

    pub(super) fn pop<T: 'static>(&mut self) -> Option<T> {
        let type_id = TypeId::of::<Vec<T>>();

        if let Some(entry) = self.inner.get_mut(&type_id) {
            let vec = entry.downcast_mut::<Vec<T>>().expect("vec exists");
            let value = vec.pop();

            if vec.is_empty() {
                self.inner.remove(&type_id);
            }

            return value;
        }

        None
    }
}
