#![doc = include_str!("../README.md")]

pub mod from_fn;
pub mod streamer;

pub use from_fn::{from_fn, try_from_fn};
pub use streamer::IntoStreamer;
