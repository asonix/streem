use streem::IntoStreamer;

fn main() -> Result<(), &'static str> {
    futures_executor::block_on(async {
        let input_stream = std::pin::pin!(streem::from_fn(|yielder| async move {
            for item in [Ok(1), Ok(2), Err("failure")] {
                yielder.yield_(item).await;
            }
        }));

        let mut input_streamer = input_stream.into_streamer();

        let output_stream = std::pin::pin!(streem::try_from_fn(|yielder| async move {
            while let Some(item) = input_streamer.try_next().await? {
                yielder.yield_ok(item).await;
            }

            Ok(())
        }));

        let mut output_streamer = output_stream.into_streamer();

        while let Some(item) = output_streamer.try_next().await? {
            println!("Got {item}");
        }

        Ok(())
    })
}
