use streem::IntoStreamer;

fn from_iter<I>(iter: impl IntoIterator<Item = I>) -> impl futures_core::Stream<Item = I>
where
    I: 'static,
{
    streem::from_fn(|yielder| async move {
        for i in iter {
            yielder.yield_(i).await;
        }
    })
}

fn main() {
    futures_executor::block_on(async {
        let stream = std::pin::pin!(from_iter(0..10));

        let mut streamer = stream.into_streamer();

        while let Some(item) = streamer.next().await {
            println!("Yielded {item}");
        }
    })
}
