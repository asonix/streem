# Streem

A simple library for creating and consuming async streams

## Usage

Add to your Cargo.toml
```bash
$ cargo add streem
```

Use in your application
```rust
use streem::IntoStreamer;

fn from_iter<I>(iter: impl IntoIterator<Item = I>) -> impl futures_core::Stream<Item = I>
where
    I: 'static,
{
    streem::from_fn(|yielder| async move {
        for i in iter {
            yielder.yield_(i).await;
        }
    })
}

fn main() {
    futures_executor::block_on(async {
        let stream = std::pin::pin!(from_iter(0..10));

        let mut streamer = stream.into_streamer();

        while let Some(item) = streamer.next().await {
            println!("Yielded {item}");
        }
    })
}
```

## Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the GPLv3.

## License

Copyright © 2023 asonix

Streem is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Streem is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of Streem.

You should have received a copy of the GNU General Public License along with Streem. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
